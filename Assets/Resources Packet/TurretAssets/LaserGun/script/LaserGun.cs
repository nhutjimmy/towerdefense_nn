﻿using UnityEngine;
using System.Collections;

public class LaserGun : MonoBehaviour {
	public float range = 50f;
	public GameObject enemy;
	public float coolDown = 1f;
	float tempCD;
	float distance;
	Transform body;
	Transform lens;
	public GameObject bullet;
	public AudioClip clip;

	GameObject go;

	// Use this for initialization
	void Start () {
		body = this.transform.GetChild (1);
		lens = body.GetChild (4);
		tempCD = coolDown;
	}

	// Update is called once per frame
	void Update () {
		if (enemy != null) {
			InRange ();
		} else {
			lens.gameObject.GetComponent<ParticleSystem> ().Stop ();
		}
	}

	void InRange() {
		distance = Vector3.Distance (enemy.transform.position, this.transform.position);
		if (distance <= range) {
			body.LookAt (enemy.transform);
			body.rotation = Quaternion.Euler (0f, body.rotation.eulerAngles.y, 0f);
			go = (GameObject)Instantiate (bullet, lens.position, lens.rotation);
			go.GetComponent<laser> ().start = lens.gameObject;
			go.GetComponent<laser> ().end = enemy;
			lens.gameObject.GetComponent<ParticleSystem> ().Play ();
			Destroy (go, 0.1f);
			FireBullet ();
		} else {
			lens.gameObject.GetComponent<ParticleSystem> ().Stop ();
			//TODO: Stop Line Renderer
		}
	}

	void FireBullet() {
		coolDown -= 2f*Time.deltaTime;
		if (coolDown <= 0) {
//			enemy.GetComponent<NormalEnemy> ().hitPoint -= 10;
			coolDown = tempCD;
		}
	}
}
