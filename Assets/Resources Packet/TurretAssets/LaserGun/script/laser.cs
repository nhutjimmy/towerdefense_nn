﻿using UnityEngine;
using System.Collections;

public class laser : MonoBehaviour {
	public LineRenderer laserLine;
	public GameObject start;
	public GameObject end;

//	public float range;
//	public float dist;
	// Use this for initialization
	void Start () {
		laserLine = GetComponent<LineRenderer> ();
		laserLine.SetWidth (1f, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		if (end != null) {
			laserLine.SetPosition (0, start.transform.position);
			laserLine.SetPosition (1, end.transform.position);
		} else {
			Destroy (gameObject);
		}
	}
}
