﻿using UnityEngine;
using System.Collections;

public class Roll : MonoBehaviour {

	float speed = 100f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Rotate (Vector3.forward, speed, Space.Self);
	}
}
