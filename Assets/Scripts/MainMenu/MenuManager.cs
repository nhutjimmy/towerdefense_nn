﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuManager : MonoBehaviour
{

	public Menu CurrentMenu;

	public void Start ()
	{
		ShowMenu (CurrentMenu);
	}

	public void ShowMenu (Menu menu)
	{
		if (CurrentMenu != null)
			CurrentMenu.IsOpen = false;

		CurrentMenu = menu;
		CurrentMenu.IsOpen = true;  
	}

	public void OpenNewMap (Button btnMap)
	{
		string strMapName = btnMap.GetComponentInChildren<Text> ().text;
		SceneManager.LoadScene (strMapName);
	}

	public void ExitThisGame ()
	{
		Application.Quit ();
	}

	public void Back(){
		SceneManager.LoadScene ("ScoreMenu");
	}
}
