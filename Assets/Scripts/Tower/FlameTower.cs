﻿using UnityEngine;
using System.Collections;

public class FlameTower : MonoBehaviour
{
	
	Transform turretTransform;
	Transform firePoint;
	public float range = 15f;
	public AudioClip clip;

	public GameObject bulletPrefab;
	public int cost = 5;

	public float damage = 2;
	public float maxDame = 6f;
	public float radius = 0;

	float fireCooldown = 0.5f;
	float fireCooldownLeft = 0;
	public AudioSource flameAudio = null;

	Enemy nearestEnemy = null;

	// Use this for initialization
	void Start ()
	{
		flameAudio = GetComponent<AudioSource> ();
		turretTransform = transform.Find ("Head"); 
		firePoint = this.transform.GetChild (2).GetChild (0).GetChild (0);
	}

	// Update is called once per frame
	void Update ()
	{
		// TODO: Optimize this!
		Enemy[] enemies = GameObject.FindObjectsOfType<Enemy> ();
//		Enemy nearestEnemy = null;
//		float dist = Mathf.Infinity;

		if (nearestEnemy == null) {

			float dist = range;
			foreach (Enemy e in enemies) {
				float d = Vector3.Distance (this.transform.position, e.transform.position);
				if (d < dist) {
					nearestEnemy = e;
					dist = d;
				}
			}
		}

		if (nearestEnemy == null) {
			firePoint.GetComponent<ParticleSystem> ().Stop ();
			flameAudio.Stop ();	
			return;
		}

		Vector3 dir = nearestEnemy.transform.position - this.transform.position;
		Quaternion lookRot = Quaternion.LookRotation (dir);

		if (Vector3.Distance (this.transform.position, nearestEnemy.transform.position) <= range) {
			turretTransform.rotation = Quaternion.Euler (0, lookRot.eulerAngles.y, 0);
			firePoint.GetComponent<ParticleSystem> ().Play ();
		} else {
			firePoint.GetComponent<ParticleSystem> ().Stop ();
			flameAudio.Stop ();
		}

		fireCooldownLeft -= Time.deltaTime;

		if (fireCooldownLeft <= 0 && dir.magnitude <= range) {


			fireCooldownLeft = fireCooldown;

			ShootAt (nearestEnemy);
		}
		if(dir.magnitude > range){
			nearestEnemy = null;
		}

	}

	void ShootAt (Enemy e)
	{

		GameObject bulletGO = (GameObject)Instantiate (bulletPrefab, firePoint.position, firePoint.rotation);

		flameAudio.volume = 0.1f;
		flameAudio.Play ();

		firePoint.GetComponent<ParticleSystem> ().Play ();
		Bullet bL = bulletGO.GetComponent<Bullet> ();
		bL.target = e.transform;
		bL.damage = damage;
		bL.radius = radius;
	}
}
