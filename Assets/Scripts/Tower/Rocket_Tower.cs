﻿using UnityEngine;
using System.Collections;

public class Rocket_Tower : MonoBehaviour {


	public AudioClip MisslileLaucher;
	Transform turretTransform;	
	public Transform[] muzzlePos;

	Transform firePointL;
	Transform firePointR;
	public float range = 40f;

	public GameObject bulletPrefab;
	public int cost = 5;

	public float damage = 10f;
	public float maxDame = 16f;
	public float radius = 0;

	public float fireCooldown = 0.1f;
	float fireCooldownLeft = 0;

	// Use this for initialization
	void Start () {

		//		firePointL = this.transform.GetChild (1).GetChild (1).GetChild (0);
		//		firePointR = this.transform.GetChild (1).GetChild (2).GetChild (0);
		turretTransform = transform.Find ("Head"); 
	}

	// Update is called once per frame
	void Update () {
		// TODO: Optimize this!
		EnemyFlyer[] enemies = GameObject.FindObjectsOfType<EnemyFlyer>();
		EnemyFlyer nearestEnemy = null;
		float dist = Mathf.Infinity;

		foreach(EnemyFlyer e in enemies){
			float d = Vector3.Distance (this.transform.position, e.transform.position);
			if(nearestEnemy == null || d < dist){
				nearestEnemy = e;
				dist = d;
			}
		}

		if(nearestEnemy == null){

			//			firePointL.GetComponentInParent<Roll> ().enabled = false;
			//			firePointR.GetComponentInParent<Roll> ().enabled = false;
			//			firePointL.GetComponent<ParticleSystem> ().Stop ();
			//			firePointR.GetComponent<ParticleSystem> ().Stop ();
			//			Debug.Log ("No enemies?");
			return;
		}

		Vector3 dir = nearestEnemy.transform.position - this.transform.position;
		Quaternion lookRot = Quaternion.LookRotation (dir);

		//		Debug.Log (lookRot.eulerAngles);
		if (Vector3.Distance (this.transform.position, nearestEnemy.transform.position) <= range) {
			//			firePointL.GetComponentInParent<Roll> ().enabled = true;
			//			firePointR.GetComponentInParent<Roll> ().enabled = true;
			turretTransform.rotation = Quaternion.Euler (0, lookRot.eulerAngles.y, 0);
		} else {
			//			firePointL.GetComponentInParent<Roll> ().enabled = false;
			//			firePointR.GetComponentInParent<Roll> ().enabled = false;
			//			firePointL.GetComponent<ParticleSystem> ().Stop ();
			//			firePointR.GetComponent<ParticleSystem> ().Stop ();
		}

		fireCooldownLeft -= Time.deltaTime;

		if (fireCooldownLeft <= 0 && dir.magnitude <= range) {

			//			Transform flameGO = this.transform.GetChild (2).transform.GetChild (0).transform.GetChild (0);
			//			Debug.Log (flameGO);
			//			ParticleSystem ps = flameGO.GetComponent<ParticleSystem> ();
			//			ps.Play ();
			fireCooldownLeft = fireCooldown;

			ShootAt (nearestEnemy);
		}
		//		else{
		//			Transform flameGO 		= this.transform.GetChild (2).transform.GetChild (0).transform.GetChild (0);
		//			Debug.Log (flameGO);
		//			ParticleSystem ps = flameGO.GetComponent<ParticleSystem>();
		//			ps.Stop ();
		//		}
	}

	void ShootAt(EnemyFlyer e){

		AudioSource audio = this.GetComponent<AudioSource> ();


		int i = Random.Range (0,2);

		audio.PlayOneShot (MisslileLaucher, 0.5f);
		GameObject newMissile = Instantiate (bulletPrefab,muzzlePos[i].position,muzzlePos[i].rotation) as GameObject;
		newMissile.GetComponent<Rocket_Bullet> ().target = e.transform;

		// TODO: Fire out the tip!
		//		ParticleSystem ps = gameObject.GetComponent<ParticleSystem>();
		//		ps.Play ();
		//		AudioSource audio = GetComponent<AudioSource>();
		//		audio.PlayOneShot (clip, 0.75f);

		//		GameObject bulletGOLeft = (GameObject)Instantiate (bulletPrefab, firePointL.position, firePointL.rotation);
		//		GameObject bulletGORight = (GameObject)Instantiate (bulletPrefab, firePointR.position, firePointR.rotation);
		//		firePointL.GetComponent<ParticleSystem> ().Play ();
		//		firePointR.GetComponent<ParticleSystem> ().Play ();
		//
		//		Bullet bL =  bulletGOLeft.GetComponent<Bullet> ();
		//		bL.target = e.transform;
		//		bL.damage = damage;
		//		bL.radius = radius;
		//
		//		Bullet bR =  bulletGORight.GetComponent<Bullet> ();
		//		bR.target = e.transform;
		//		bR.damage = damage;
		//		bR.radius = radius;
	}
}
