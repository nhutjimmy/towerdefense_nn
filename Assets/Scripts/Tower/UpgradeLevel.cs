﻿using UnityEngine;
using System.Collections;

public class UpgradeLevel : MonoBehaviour
{
	
	public GameObject circle;
	GameObject goTurretCircle;
	float towerRadius;

	void OnMouseDown ()
	{
		UpgradePanelIn ();

		GameObject goController = GameObject.Find ("_Controller_");
		goController.GetComponent<BuildingManager> ().towerToUpgrade = this.transform.gameObject;
		goController.GetComponent<BuildingManager> ().towerToUpgradeID = TurretID ();
		//get the range of the tower
		if (TurretID () != -1)
			towerRadius = goController.GetComponent<BuildingManager> ().allTurretRange [TurretID ()];

		goTurretCircle = (GameObject)Instantiate (circle, new Vector3 (this.transform.position.x, this.transform.position.y + 1f, this.transform.position.z), this.transform.rotation);
		goTurretCircle.GetComponent<DrawCircle> ().radius = towerRadius;
		goTurretCircle.transform.Rotate (Vector3.left, 90f);


	}

	void OnMouseUp ()
	{
		Destroy (goTurretCircle);
	}

	//custom function
	public void UpgradePanelIn ()
	{
		Animator ani = GameObject.Find ("Canvas").transform.GetChild (1).gameObject.GetComponent<Animator> ();
		ani.enabled = true;
		ani.Play ("UpgradePanelIn");
	}

	public int TurretID ()
	{
		int i = -1;
		if (this.gameObject) {
			if (this.GetComponent<GatlingGunTower> ()) {
				i = 0;
			} else if (this.GetComponent<FlameTower> ()) {
				i = 1;
			} else if (this.GetComponent<CannonTower> ()) {
				i = 2;
			} else if (this.GetComponent<Rocket_Tower> ()) {
				i = 3;
			} else if (this.GetComponent<LaserTower> ()) {
				i = 4;
			}
		}
		return i;
	}
}
