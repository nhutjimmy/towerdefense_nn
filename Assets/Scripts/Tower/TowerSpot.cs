﻿using UnityEngine;
using System.Collections;

public class TowerSpot : MonoBehaviour
{
	void OnMouseUp ()
	{

		BuildingManager bm = GameObject.FindObjectOfType<BuildingManager> ();

		if (bm.selectedTower != null) {
			ScoreManager sm = GameObject.FindObjectOfType<ScoreManager> ();

			// Gatling Gun
			if (bm.selectedTower.GetComponent<GatlingGunTower> () != null) {
				if (sm.money < bm.selectedTower.GetComponent<GatlingGunTower> ().cost) {
					Debug.Log ("Not enough money");
					return;
				} else {

					sm.money -= bm.selectedTower.GetComponent<GatlingGunTower> ().cost;
				}
			}

			//Flame Tower
			if (bm.selectedTower.GetComponent<FlameTower> () != null) {
				if (sm.money < bm.selectedTower.GetComponent<FlameTower> ().cost) {
					Debug.Log ("Not enough money");
					return;
				} else {
					sm.money -= bm.selectedTower.GetComponent<FlameTower> ().cost;
				}
			}

			//Rocket Tower
			if (bm.selectedTower.GetComponent<Rocket_Tower> () != null) {
				if (sm.money < bm.selectedTower.GetComponent<Rocket_Tower> ().cost) {
					Debug.Log ("Not enough money");
					return;
				} else {
					sm.money -= bm.selectedTower.GetComponent<Rocket_Tower> ().cost;
				}
			}

			//Cannon Tower
			if (bm.selectedTower.GetComponent<CannonTower> () != null) {
				if (sm.money < bm.selectedTower.GetComponent<CannonTower> ().cost) {
					Debug.Log ("Not enough money");
					return;
				} else {
					sm.money -= bm.selectedTower.GetComponent<CannonTower> ().cost;
				}
			}

			//Laser Tower
			if (bm.selectedTower.GetComponent<LaserTower> () != null) {
				if (sm.money < bm.selectedTower.GetComponent<LaserTower> ().cost) {
					Debug.Log ("Not enough money");
					return;
				} else {
					sm.money -= bm.selectedTower.GetComponent<LaserTower> ().cost;
				}
			}
			// FIXME: Right now  we assume that we're an object nested in a parent.


			Instantiate (bm.selectedTower, transform.position, transform.rotation);
			bm.selectedTower = null;
//			Destroy (transform.parent.gameObject);
		}
	}
}
