﻿using UnityEngine;
using System.Collections;

public class CannonTower : MonoBehaviour
{

	Transform turretTransform;
	public GameObject cannon;
	public Transform shotpoint;

	public float range = 30f;
	public AudioClip clip;

	//	public GameObject bulletPrefab;

	public int cost = 5;

	public float damage = 100f;
	public float maxDame = 106f;
	public float radius = 0;

	float fireCooldown = 1f;
	float fireCooldownLeft = 0;

	// Use this for initialization
	void Start ()
	{
		turretTransform = transform.Find ("Head"); 
	}

	// Update is called once per frame
	void Update ()
	{
		// TODO: Optimize this!
		Enemy[] enemies = GameObject.FindObjectsOfType<Enemy> ();
		Enemy nearestEnemy = null;
		float dist = Mathf.Infinity;

		foreach (Enemy e in enemies) {
			float d = Vector3.Distance (this.transform.position, e.transform.position);
			if (nearestEnemy == null || d < dist) {
				nearestEnemy = e;
				dist = d;
			}
		}

		if (nearestEnemy == null) {
			shotpoint.gameObject.GetComponent<ParticleSystem> ().Stop ();
			return;
		}

		Vector3 dir = nearestEnemy.transform.position - this.transform.position;
		Quaternion lookRot = Quaternion.LookRotation (dir);

		if (Vector3.Distance (this.transform.position, nearestEnemy.transform.position) <= range) {
			turretTransform.rotation = Quaternion.Euler (0, lookRot.eulerAngles.y, 0);
		} else {
			shotpoint.gameObject.GetComponent<ParticleSystem> ().Stop ();
		}

		fireCooldownLeft -= Time.deltaTime;

		if (fireCooldownLeft <= 0 && dir.magnitude <= range) {

			fireCooldownLeft = fireCooldown;

			ShootAt (nearestEnemy);
		}
	}

	void ShootAt (Enemy e)
	{

		GameObject go = (GameObject)Instantiate (cannon, shotpoint.position, shotpoint.rotation);
		shotpoint.gameObject.GetComponent<ParticleSystem> ().Play ();
		AudioSource audio = GetComponent<AudioSource> ();
		audio.PlayOneShot (clip, 0.5f);
		go.GetComponent<Cannon_Bullet> ().enemy = e;
		go.GetComponent<Cannon_Bullet> ().cannonLauncher = this.transform;
		go.GetComponent<Cannon_Bullet> ().pos = e.transform.position;
		// TODO: Fire out the tip!
		//		ParticleSystem ps = gameObject.GetComponent<ParticleSystem>();
		//		ps.Play ();
		//		AudioSource audio = GetComponent<AudioSource>();
		//		audio.PlayOneShot (clip, 0.75f);

		//		GameObject bulletGOLeft = (GameObject)Instantiate (bulletPrefab, firePointL.position, firePointL.rotation);
		//		GameObject bulletGORight = (GameObject)Instantiate (bulletPrefab, firePointR.position, firePointR.rotation);
		//		firePointL.GetComponent<ParticleSystem> ().Play ();
		//		firePointR.GetComponent<ParticleSystem> ().Play ();
		//
		//		Bullet bL =  bulletGOLeft.GetComponent<Bullet> ();
		//		bL.target = e.transform;
		//		bL.damage = damage;
		//		bL.radius = radius;
		//
		//		Bullet bR =  bulletGORight.GetComponent<Bullet> ();
		//		bR.target = e.transform;
		//		bR.damage = damage;
		//		bR.radius = radius;
	}
}

