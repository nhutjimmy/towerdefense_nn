﻿using UnityEngine;
using System.Collections;

public class Cannon_Bullet : MonoBehaviour
{
	public Enemy enemy;
	public float dame = 10f;
	public Transform cannonLauncher;
	public float angle = 45.0f;
	public float gravity = 9.8f;
	public Vector3 pos;

	// Use this for initialization
	void Start ()
	{
		StartCoroutine (ThrowCannon ());
	}

	// Update is called once per frame
	void Update ()
	{
		if (enemy == null) {
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.tag == "Enemy") {
			Enemy[] enemies = GameObject.FindObjectsOfType<Enemy> ();
			foreach (Enemy e in enemies) {
				if (Vector3.Distance (e.transform.position, enemy.transform.position) <= 20f) {
					e.TakeDamage (dame);
				}
			}
			Destroy (gameObject);
		}
		if (col.tag == "Map") {
			Enemy[] enemies = GameObject.FindObjectsOfType<Enemy> ();
			foreach (Enemy e in enemies) {
				if (Vector3.Distance (this.transform.position, enemy.transform.position) <= 20f) {
					e.TakeDamage (dame);
				}
			}
			Destroy (gameObject);
		}
	}

	IEnumerator ThrowCannon ()
	{

		float distance = Vector3.Distance (this.transform.position, enemy.transform.position);

		float vel = distance / (Mathf.Sin (2 * angle * Mathf.Deg2Rad) / gravity);

		float vX = Mathf.Sqrt (vel) * Mathf.Cos (angle * Mathf.Deg2Rad);
		float vY = Mathf.Sqrt (vel) * Mathf.Sin (angle * Mathf.Deg2Rad);

		float flightDuration = distance / vX;

		this.transform.rotation = Quaternion.LookRotation (enemy.transform.position - this.transform.position);

		float elapse_time = 0;

		while (elapse_time < flightDuration) {
			this.transform.Translate (0, (vY - (gravity * elapse_time)) * Time.deltaTime, vX * Time.deltaTime);

			elapse_time += Time.deltaTime;

			yield return null;
		}
	}
}
