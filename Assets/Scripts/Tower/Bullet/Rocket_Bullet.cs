﻿using UnityEngine;
using System.Collections;

public class Rocket_Bullet : MonoBehaviour
{

	public float speed = 50f;
	public Transform target;
	public float damage = 1f;
	public float radius = 0;

	// Update is called once per frame
	void Update ()
	{
		if (target == null) {
			// Out enemy went away!
			Destroy (gameObject);
			return;
		}


		Vector3 dir = target.transform.GetChild (0).position - this.transform.localPosition;

		float distThisFrame = speed * Time.deltaTime;

		if (dir.magnitude <= distThisFrame) {
			// We reached the target
			DoBulletHit ();
		} else {
			// TODO: Consider ways to smooth this motion.

			// Move towards node
			transform.Translate (dir.normalized * distThisFrame, Space.World);
			Quaternion targetRotation = Quaternion.LookRotation (dir);
			this.transform.rotation = Quaternion.Lerp (this.transform.rotation, targetRotation, Time.deltaTime * 5);
		}
	}

	void DoBulletHit ()
	{
		// TODO: What if it's an exploding bullet with an area of effect?
		if (radius == 0) {
			target.GetComponent<EnemyFlyer> ().TakeDamage (damage);
		} else {
			Collider[] cols = Physics.OverlapSphere (transform.position, radius);

			foreach (Collider c in cols) {
				EnemyFlyer e = c.GetComponent<EnemyFlyer> ();
				if (e != null) {
					// TODO: You COULD do a falloff of damage based on distance, but that's rare for TD games
					e.GetComponent<EnemyFlyer> ().TakeDamage (damage);
				}
			}
		}
		//TODO: Maybe spawn a cool "explosion" object here

		Destroy (gameObject);
	}
}
