﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyFlyer : MonoBehaviour
{
	
	private Image myBlood;//this is the blood bar of this enemy

	public float maxHealth;//this is the max health of this enemy
	public float health = 3f;//this is the health of this enemy

	public float Health { get { return health; } }//get set function of the Health

	GameObject pathGO;//path game object

	public AudioClip clip;//audio clip of the enemy
	Transform targetPathNode;//path node of the enemy
	int pathNodeIndex = 0;//create the first value of the pathNodeIndex

	float speed = 5f;//speed of the enemy moving

	public int moneyValue = 1;//the cash when the enemy was died

	// Use this for initialization
	void Start ()
	{
		
		pathGO = GameObject.Find ("FlyerPath");//get the path for enemy
		myBlood = this.transform.FindChild ("BloodBar").FindChild ("BloodDie").FindChild ("BloodLive").GetComponent<Image> ();
	}

	void GetNextPathNode ()
	{
		
		if (pathNodeIndex < pathGO.transform.childCount) {
			targetPathNode = pathGO.transform.GetChild (pathNodeIndex);
			pathNodeIndex++;
		} else {
			targetPathNode = null;
			ReachedGoal ();
		}
	}

	// Update is called once per frame
	void Update ()
	{
		
		if (targetPathNode == null) {
			GetNextPathNode ();
			if (targetPathNode == null) {
				// We've run out of path!
//				ReachedGoal ();
				return;
			}
		}

		Vector3 dir = targetPathNode.position - this.transform.localPosition;
		float distThisFrame = speed * Time.deltaTime;

		if (dir.magnitude <= distThisFrame) {
			// We reached the node
			targetPathNode = null;
		} else {
			// TODO: Consider ways to smooth this motion.
			// Move towards node
			transform.Translate (dir.normalized * distThisFrame, Space.World);
			Quaternion targetRotation = Quaternion.LookRotation (dir);
			this.transform.rotation = Quaternion.Lerp (this.transform.rotation, targetRotation, Time.deltaTime * 5);
		}

	}

	void ReachedGoal ()
	{
		
		GameObject.FindObjectOfType<ScoreManager> ().LoseLife ();
		GameObject.FindObjectOfType<allEnemys> ().numbersOfEnemy -= 1;
		Destroy (gameObject);
	}

	public void TakeDamage (float damage)
	{
		
		health -= damage;
		myBlood.fillAmount = health / maxHealth;

		if (health <= 0) {
			Die ();	
			GameObject.FindObjectOfType<allEnemys> ().numbersOfEnemy -= 1;
		}
	}

	public void Die ()
	{

		// TODO: Do this more safely!
		GameObject.FindObjectOfType<ScoreManager> ().money += moneyValue;

		Destroy (gameObject);
	}
}
