﻿using UnityEngine;
using System.Collections;

public class CameraFacing : MonoBehaviour {

	private Camera myCamera;

	// Use this for initialization
	void Start () {
		myCamera = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (transform.position + myCamera.transform.rotation * Vector3.forward, myCamera.transform.rotation * Vector3.up);
	}
}
