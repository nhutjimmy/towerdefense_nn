﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;



public class EnemyTommy : MonoBehaviour
{
	public float spawnCD = 1f;
	// the distance between enemies
	public float spawnCDremaining = 30f;
	public WaveComponent wcTemp;
	public GameObject[] allEnemys;
	public ScoreManager sm;

	[System.Serializable]
	public class WaveComponent
	{
		public GameObject enemyPrefab;
		public int num;
		[System.NonSerialized]  
		public int spawned = 0;
	}

	public WaveComponent[] waveComps;

	// Update is called once per frame
	void Update ()
	{
		spawnCDremaining -= Time.deltaTime;
		sm = GameObject.FindObjectOfType<ScoreManager> ();
		sm.timeRemaining = spawnCDremaining;

		if (spawnCDremaining < 0) {
			spawnCDremaining = spawnCD;

			bool didSpawn = false;

			// Go through the wave comps until we find something to spawn;
			foreach (WaveComponent wc in waveComps) {

				if (wc.spawned < wc.num) {
					//Spawn it!

					wc.spawned++;
					wcTemp = wc;

					IncreaseHealthOfEnemy (wc.enemyPrefab);
					Instantiate (wc.enemyPrefab, this.transform.position, this.transform.rotation);
					didSpawn = true;
					break;
				}
			}
			if (didSpawn == false) {
				// Wave must be complete!
				// TODO: Instantiate next wave object!
				if (transform.parent.childCount > 1) {
					transform.parent.GetChild (1).gameObject.SetActive (true);
				} else {
					
				}
				Destroy (gameObject);

			}
		}
	}

	public void IncreaseHealthOfEnemy (GameObject enemyGO)
	{
		float fEnemyHealth = 100;

		if (SceneManager.GetActiveScene ().buildIndex == 1) {
			fEnemyHealth = 100;
			enemyGO.GetComponent<Enemy> ().health = fEnemyHealth;
			enemyGO.GetComponent<Enemy> ().maxHealth = fEnemyHealth;
		} else if (SceneManager.GetActiveScene ().buildIndex == 2) {
			fEnemyHealth = 200;
			enemyGO.GetComponent<Enemy> ().health = fEnemyHealth;
			enemyGO.GetComponent<Enemy> ().maxHealth = fEnemyHealth;
		} else if (SceneManager.GetActiveScene ().buildIndex == 3) {
			fEnemyHealth = 300;
			enemyGO.GetComponent<Enemy> ().health = fEnemyHealth;
			enemyGO.GetComponent<Enemy> ().maxHealth = fEnemyHealth;
		} else if (SceneManager.GetActiveScene ().buildIndex == 4) {
			fEnemyHealth = 400;
			if (enemyGO.GetComponent<Enemy> ()) {
				enemyGO.GetComponent<Enemy> ().health = fEnemyHealth;
				enemyGO.GetComponent<Enemy> ().maxHealth = fEnemyHealth;
			}
		} else if (SceneManager.GetActiveScene ().buildIndex == 5) {
			fEnemyHealth = 500;
			enemyGO.GetComponent<Enemy> ().health = fEnemyHealth;
			enemyGO.GetComponent<Enemy> ().maxHealth = fEnemyHealth;
		}

	}
}
