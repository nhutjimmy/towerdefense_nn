﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{

	GameObject pathGO;
	public AudioClip clip;
	Transform targetPathNode;
	int pathNodeIndex = 0;

	float speed = 5f;

	public int moneyValue = 10;

	public float maxHealth;
	public float health = 1f;
	private Image myBloodBar;

	public float Health { get { return health; } }

	void Start ()
	{
		
		pathGO = GameObject.Find ("GroundPath");
		myBloodBar = this.transform.FindChild ("BloodBar").FindChild ("BloodDie").FindChild ("BloodLive").GetComponent<Image> ();
	}

	void GetNextPathNode ()
	{
		
		if (pathNodeIndex < pathGO.transform.childCount) {
			targetPathNode = pathGO.transform.GetChild (pathNodeIndex);
			pathNodeIndex++;
		} else {
			targetPathNode = null;
			ReachedGoal ();
		}
	}

	// Update is called once per frame
	void Update ()
	{
		if (targetPathNode == null) {
			GetNextPathNode ();
			if (targetPathNode == null) {
				// We've run out of path!
//				ReachedGoal();
				return;
			}
		}

		Vector3 dir = targetPathNode.position - this.transform.localPosition;
		float distThisFrame = speed * Time.deltaTime;

		if (dir.magnitude <= distThisFrame) {
			// We reached the node
			targetPathNode = null;
		} else {
			
			// TODO: Consider ways to smooth this motion.
			// Move towards node
			transform.Translate (dir.normalized * distThisFrame, Space.World);
			Quaternion targetRotation = Quaternion.LookRotation (dir);
			this.transform.rotation = Quaternion.Lerp (this.transform.rotation, targetRotation, Time.deltaTime * 5);
		}

	}

	void ReachedGoal ()
	{
		
		GameObject.FindObjectOfType<ScoreManager> ().LoseLife ();
		GameObject.FindObjectOfType<allEnemys> ().numbersOfEnemy -= 1;
		Destroy (gameObject);
	}

	public void TakeDamage (float damage)
	{
		

		if (health <= 0) {
			Die ();
			GameObject.FindObjectOfType<allEnemys> ().numbersOfEnemy -= 1;
		}
		health -= damage;
		myBloodBar.fillAmount = health / maxHealth;
	}

	public void Die ()
	{

		// TODO: Do this more safely!

		GameObject.FindObjectOfType<ScoreManager> ().money += moneyValue;
		AudioSource audio = GetComponent<AudioSource> ();
		audio.enabled = true;
		audio.PlayOneShot (clip, 1f);

		Destroy (gameObject);

	}
}
