﻿using UnityEngine;
using System.Collections;
using System;
using System.Data;
using Mono.Data.Sqlite;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public class HighScoreManager : MonoBehaviour {


	public GameObject scorePrfab;

	private string strConnect;
	private List<HighScore> highScores = new List<HighScore> ();
	public Transform scoreParent;

	// Use this for initialization
	void Start () {
		strConnect = "URI=file:" + Application.dataPath + "/db/HighScoreDB.sqlite";
		ShowScores ();
//		InsertScore ("Nhuttommy",69);
//		DeleteScore(4);
//		GetScores ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void InsertScore(string name, int newScore){
		using(IDbConnection dbConnection = new SqliteConnection(strConnect)){
			dbConnection.Open ();

			using(IDbCommand dbCmd = dbConnection.CreateCommand()){
				string sqlQuery = string.Format("insert into tbl_HighScores(Name, Score) Values(\"{0}\",\"{1}\")",name, newScore);
				dbCmd.CommandText = sqlQuery;
				dbCmd.ExecuteScalar ();
				dbConnection.Close ();
			}
		}
	}

	private void GetScores(){

		highScores.Clear ();
		//select * from hightscores order by score desc limit 5
		using(IDbConnection dbConnection = new SqliteConnection(strConnect)){
			dbConnection.Open ();

			using(IDbCommand dbCmd = dbConnection.CreateCommand()){
				string sqlQuery = "select * from tbl_HighScores order by score desc limit 10";//"SELECT * from tbl_HighScores";
				dbCmd.CommandText = sqlQuery;

				using(IDataReader reader = dbCmd.ExecuteReader()){
					while(reader.Read()){
						Debug.Log (reader.GetString(1) +" - "+reader.GetInt32(2));
						highScores.Add(new HighScore(reader.GetInt32(0), reader.GetInt32(2),reader.GetString(1),reader.GetDateTime(3)));
					}
					dbConnection.Close ();
					reader.Close ();
				}
			}
		}
	}

	private void DeleteScore(int iID){
		using(IDbConnection dbConnection = new SqliteConnection(strConnect)){
			dbConnection.Open ();

			using(IDbCommand dbCmd = dbConnection.CreateCommand()){
				string sqlQuery = string.Format("delete from tbl_HighScores where playerID = \"{0}\"",iID);
				dbCmd.CommandText = sqlQuery;
				dbCmd.ExecuteScalar ();
				dbConnection.Close ();
			}
		}
	}

	private void ShowScores(){
		GetScores ();
		Debug.Log (highScores.Count);
		for(int i = 0; i < highScores.Count; i++){
			GameObject tmpObjec = Instantiate (scorePrfab);

			HighScore tmpScore	= highScores [i];

			Debug.Log (tmpScore.Name);
			tmpObjec.GetComponent<HighScoreScript> ().SetScore (tmpScore.Name,tmpScore.Score.ToString(), "#" + ((i+1).ToString()),tmpScore.Date.ToShortDateString());
		
			tmpObjec.transform.SetParent (scoreParent);
		}
	}

	public void Back(){
		SceneManager.LoadScene ("MainMenu");
	}
}
