﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighScoreScript : MonoBehaviour {

	public GameObject score;
	public GameObject scoreName;
	public GameObject rank;
	public GameObject date;

	public void SetScore(string name, string score, string rank, string date){
		this.rank.GetComponent<Text> ().text = rank;
		this.scoreName.GetComponent<Text> ().text = name;
		this.score.GetComponent<Text> ().text = score;
		this.date.GetComponent<Text> ().text = date;
	}

}
