﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class BuildingManager : MonoBehaviour
{

	public GameObject[] allTowers;
	public int[] allTurretCost;
	public float[] allTurretRange;
	public ScoreManager sm;
	public GameObject selectedTower;
	public Canvas pauseMenu;
	public GameObject towerToUpgrade;
	public int towerToUpgradeID = 0;

	private int structureIndex = 0;

	// Use this for initialization
	void Start ()
	{

		structureIndex = 0;
		pauseMenu.enabled = false;
	}

	public void SelectTowerType (GameObject prefab)
	{

		string btnName = prefab.name;
		if (btnName == "btnGatling") {
			structureIndex = 0;
		} else if (btnName == "btnFlameTower") {
			structureIndex = 1;
		} else if (btnName == "btnCannon") {
			structureIndex = 2;
		} else if (btnName == "btnMissileLaucher") {
			structureIndex = 3;
		} else if (btnName == "btnLaser") {
			structureIndex = 4;
		}

		selectedTower = allTowers [structureIndex];
	}

	public void OpenPauseMenu ()
	{
		pauseMenu.enabled = true;
		Time.timeScale = 0;
	}

	public void ResumeToGame ()
	{
		pauseMenu.enabled = false;
		Time.timeScale = 1;
	}

	public void ExitGame ()
	{
		Application.Quit ();
	}

	public void BackToMainMenu ()
	{
		SceneManager.LoadScene ("MainMenu");
		Time.timeScale = 1;
	}

	public void RestartScence ()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		Time.timeScale = 1;
	}

	public void UpgradeTurret ()
	{
		sm = GameObject.FindObjectOfType<ScoreManager> ();

		switch (towerToUpgradeID) {
		case 0:
			if (sm.money >= (allTurretCost [0] + allTurretCost [0] / 2)) {
				if (towerToUpgrade.GetComponent<GatlingGunTower> ().damage < allTowers [0].GetComponent<GatlingGunTower> ().maxDame) {
					towerToUpgrade.GetComponent<GatlingGunTower> ().damage += 2;
					sm.money -= (allTurretCost [0] + allTurretCost [0] / 2);
				}
			}
			break;
		case 1:
			if (sm.money >= (allTurretCost [1] + allTurretCost [1] / 2)) {
				if (towerToUpgrade.GetComponent<FlameTower> ().damage < allTowers [1].GetComponent<FlameTower> ().maxDame) {
					towerToUpgrade.GetComponent<FlameTower> ().damage += 2;
					sm.money -= (allTurretCost [1] + allTurretCost [1] / 2);
				}
			}
			break;
		case 2:
			if (sm.money >= (allTurretCost [2] + allTurretCost [2] / 2)) {
				if (towerToUpgrade.GetComponent<CannonTower> ().damage < allTowers [2].GetComponent<CannonTower> ().maxDame) {
					towerToUpgrade.GetComponent<CannonTower> ().damage += 2;
					sm.money -= (allTurretCost [2] + allTurretCost [2] / 2);
				}
			}
			break;
		case 3:
			if (sm.money >= (allTurretCost [3] + allTurretCost [3] / 2)) {
				if (towerToUpgrade.GetComponent<Rocket_Tower> ().damage < allTowers [3].GetComponent<Rocket_Tower> ().maxDame) {
					towerToUpgrade.GetComponent<Rocket_Tower> ().damage += 2;
					sm.money -= (allTurretCost [3] + allTurretCost [3] / 2);
				}
			}
			break;
		case 4:
			if (sm.money >= (allTurretCost [4] + allTurretCost [4] / 2)) {
				if (towerToUpgrade.GetComponent<LaserTower> ().damage < allTowers [4].GetComponent<LaserTower> ().maxDame) {
					towerToUpgrade.GetComponent<LaserTower> ().damage += 2;
					sm.money -= (allTurretCost [4] + allTurretCost [4] / 2);
				}
			}
			break;
		default:
			return;
		}


		UpgradePanelOut ();
	}

	public void SellTurret ()
	{
		UpgradePanelOut ();
		sm = GameObject.FindObjectOfType<ScoreManager> ();
		switch (towerToUpgradeID) {
		case 0:
			sm.money += allTurretCost [towerToUpgradeID] / 2;
			break;
		case 1:
			sm.money += allTurretCost [towerToUpgradeID] / 2;
			break;
		case 2:
			sm.money += allTurretCost [towerToUpgradeID] / 2;
			break;
		case 3:
			sm.money += allTurretCost [towerToUpgradeID] / 2;
			break;
		case 4:
			sm.money += allTurretCost [towerToUpgradeID] / 2;
			break;
		default:
			return;
		}
		Destroy (towerToUpgrade);
	}

	public void CancelUpgrade ()
	{
		UpgradePanelOut ();
	}

	public void UpgradePanelOut ()
	{
		Animator ani = GameObject.Find ("Canvas").transform.GetChild (1).gameObject.GetComponent<Animator> ();
		ani.enabled = true;
		ani.Play ("UpgradePanelOut");
	}
}
