﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Data;
using Mono.Data.Sqlite;
using System.Collections.Generic;

public class ScoreManager : MonoBehaviour
{

	private string strConnect;
	public float timeRemaining;

	public int lives = 20;
	public int money = 100;

	public Text moneyText;
	public Text livesText;
	public GameObject btnNextStage;

	public Canvas infoPanel;
	public Canvas pauseMenu;

	public Text infoText;
	public Text remainingTime;
	public Text warning;

	public string stringTemp;

	public void LoseLife (int l = 1)
	{
		lives -= 1;
		if (lives <= 0) {
			InsertScore ("Nhut", money);// chua lay tien qua
			GameOver ();
		}
	}

	void Start(){
		stringTemp = "";
		strConnect = "URI=file:" + Application.dataPath + "/db/HighScoreDB.sqlite";
	}

	public void GameOver ()
	{
		pauseMenu.enabled = true; //turn game over menu on
		pauseMenu.transform.GetChild (0).transform.GetChild (0).gameObject.SetActive (false);//turn the resume game button off
		pauseMenu.transform.GetChild(0).GetChild(4).gameObject.SetActive(true);
		Time.timeScale = 0;//pause game
	}

	void Update ()
	{
		if ((int)timeRemaining > 0) {
			remainingTime.enabled = true;
			remainingTime.text = ((int)timeRemaining).ToString () + "s";
			if(warning){
			warning.enabled = true;
				warning.text = "Warning! Flyer enemies! This message will disappear in " + remainingTime.text;}
		} else if ((int)timeRemaining  == 0){
			remainingTime.enabled = false;
			if (warning) {
				warning.enabled = false;
			}
		}
			
		if (GameObject.FindObjectOfType<allEnemys> ().numbersOfEnemy <= 0) {
			infoPanel.gameObject.SetActive (true);
			infoText.text = "You are victory";
		}

		moneyText.text = "Money: " + money.ToString ();
		livesText.text = "Lives: " + lives.ToString ();
	}

	public void GoToNextStage(){
		int iBuildIndex = SceneManager.GetActiveScene ().buildIndex;
		if (iBuildIndex == 5) {
			iBuildIndex = 1;
		} else {
			iBuildIndex += 1;
			SceneManager.LoadScene (iBuildIndex);
		}

	}
		
	public void TangTocManChoi (Text txtButtonSpeedName)
	{
		string strNormalSpeedPlay = "1x";
		string strDoubleSpeedPlay = "2x";
		string strTripleSpeedPlay = "3x";
		string strQuatraSpeedPlay = "4x";
		string strSpeedName = txtButtonSpeedName.text;

		if (strSpeedName == strNormalSpeedPlay) {
			Time.timeScale = 2f;
			txtButtonSpeedName.text = "2x";
		} else if (strSpeedName == strDoubleSpeedPlay) {
			Time.timeScale = 3f;
			txtButtonSpeedName.text = "3x";
		} else if (strSpeedName == strTripleSpeedPlay) {
			Time.timeScale = 4f;
			txtButtonSpeedName.text = "4x";
		} else if (strSpeedName == strQuatraSpeedPlay) {
			Time.timeScale = 1f;
			txtButtonSpeedName.text = "1x";
		}

	}

	public void InsertScore(string name, int newScore){
		using(IDbConnection dbConnection = new SqliteConnection(strConnect)){
			dbConnection.Open ();

			using(IDbCommand dbCmd = dbConnection.CreateCommand()){
				string sqlQuery = string.Format("insert into tbl_HighScores(Name, Score) Values(\"{0}\",\"{1}\")",name, newScore);
				dbCmd.CommandText = sqlQuery;
				dbCmd.ExecuteScalar ();
				dbConnection.Close ();
			}
		}
	}
}
